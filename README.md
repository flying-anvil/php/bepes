# Bepes

BPS patcher.  
Curently only for applying patches, not for creating (feel free to contribute if you want that feature).

Thanks to _byuu_ for [documenting the BPS file specifications](https://github.com/blakesmith/rombp/blob/master/docs/bps_spec.md)

## Usage

There are two bundled patcher classes:

`\FlyingAnvil\Bepes\Patcher\FileBpsPatcher` writes directly to the output file. This is very slow, but saveds on memory.

`\FlyingAnvil\Bepes\Patcher\MemoryBpsPatcher` creates the result in-memory, before writing it into a file. This is very fast, but uses more memory
(at least the size of the whole result file);

### Benchmarks

| Patcher            | Duration (ms) | Memory (bytes) |
|--------------------|--------------:|----------------|
| `FileBpsPatcher`   |      21664.83 | 2097152        |
| `MemoryBpsPatcher` |       1174.26 | 4198400        |
|                    |               |                |

## Disclaimer

This if not a final/finished version. Expect breaking changes.
