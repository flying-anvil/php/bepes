<?php

declare(strict_types=1);

namespace FlyingAnvil\Bepes\Patcher;

use FlyingAnvil\Bepes\BpsHelper;
use FlyingAnvil\Bepes\BpsPatch;
use FlyingAnvil\Bepes\DataObject\BpsCommand;
use FlyingAnvil\Bepes\DataObject\BpsCommandType;
use FlyingAnvil\Bepes\Exception\PatchSourceException;
use FlyingAnvil\Bepes\Exception\PatchTargetException;
use FlyingAnvil\Libfa\Wrapper\File;

class FileBpsPatcher implements BpsPatcherInterface
{
    public function applyPatch(BpsPatch $patch, string $sourceFilePath, string $targetFilePath, ?callable $onProgress = null): void
    {
        $sourceHash = hash_file('crc32b', $sourceFilePath);
        $metadata = $patch->getMetadata();
        if (hexdec($sourceHash) !== $metadata->getChecksumSource()) {
            throw new PatchSourceException('The patch is not intended for this source');
        }

        $patchFile = $patch->getPatchFile();
        $source = File::load($sourceFilePath);
        $target = File::load($targetFilePath);

        $source->open('rb');
        $target->open('w+b');

        $offsetSourceRelative = 0;
        $offsetTargetRelative = 0;

        $onProgress && $onProgress(0, $metadata->getTargetSize());

        foreach ($patch->iterateCommands() as $command) {
            switch ($command->getCommandType()) {
                case BpsCommandType::SourceRead:
                    $this->sourceRead($command, $source, $target);
                    break;
                case BpsCommandType::TargetRead:
                    $this->tragetRead($command, $patchFile, $target);
                    break;
                case BpsCommandType::SourceCopy:
                    $this->sourceCopy($command, $patchFile, $source, $target, $offsetSourceRelative);
                    break;
                case BpsCommandType::TargetCopy:
                    $this->targetCopy($command, $patchFile, $target, $offsetTargetRelative);
                    break;
            }

            /** @noinspection DisconnectedForeachInstructionInspection */
            $onProgress && $onProgress($target->tell(), $metadata->getTargetSize());
        }

        $onProgress && $onProgress($metadata->getTargetSize(), $metadata->getTargetSize());

        $targetHash = hash_file('crc32b', $sourceFilePath);
        if (hexdec($targetHash) !== $metadata->getChecksumSource()) {
            throw new PatchTargetException('The patch was not applied successfully: Checksum mismatch');
        }
    }

    private function sourceRead(BpsCommand $command, File $source, File $target): void
    {
        $length = $command->getLength();

        while ($length--) {
            $offset = $target->tell();
            $source->seek($offset);

            $target->write($source->readChar());
        }
    }

    private function tragetRead(BpsCommand $command, File $patchFile, File $target): void
    {
        $length = $command->getLength();

        while ($length--) {
            $target->write($patchFile->readChar());
        }
    }

    private function sourceCopy(BpsCommand $command, File $patchFile, File $source, File $target, int &$offsetSourceRelative): void
    {
        $data = BpsHelper::decodeNumber($patchFile);

        // Lowest byte indicates sign
        $offsetSourceRelative += ($data & 1 ? -1 : +1) * ($data >> 1);

        $length = $command->getLength();

        while ($length--) {
            $source->seek($offsetSourceRelative++);
            $target->write($source->readChar());
        }
    }

    private function targetCopy(BpsCommand $command, File $patchFile, File $target, int &$offsetTargetRelative): void
    {
        $data = BpsHelper::decodeNumber($patchFile);

        // Lowest byte indicates sign
        $offsetTargetRelative += ($data & 1 ? -1 : +1) * ($data >> 1);

        $length = $command->getLength();

        while ($length--) {
            $oldPosition = $target->tell();
            $target->seek($offsetTargetRelative++);
            $byte = $target->readChar();

            if ($byte === false) {
                var_dump($offsetTargetRelative - 1);
            }

            $target->seek($oldPosition);
            $target->write($byte);
        }
    }
}
