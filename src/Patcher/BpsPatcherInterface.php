<?php

declare(strict_types=1);

namespace FlyingAnvil\Bepes\Patcher;

use FlyingAnvil\Bepes\BpsPatch;

interface BpsPatcherInterface
{
    public function applyPatch(BpsPatch $patch, string $sourceFilePath, string $targetFilePath, ?callable $onProgress = null): void;
}
