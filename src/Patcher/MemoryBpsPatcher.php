<?php

declare(strict_types=1);

namespace FlyingAnvil\Bepes\Patcher;

use FlyingAnvil\Bepes\BpsHelper;
use FlyingAnvil\Bepes\BpsPatch;
use FlyingAnvil\Bepes\DataObject\BpsCommand;
use FlyingAnvil\Bepes\DataObject\BpsCommandType;
use FlyingAnvil\Bepes\Exception\PatchSourceException;
use FlyingAnvil\Bepes\Exception\PatchTargetException;
use FlyingAnvil\Libfa\Wrapper\File;

class MemoryBpsPatcher implements BpsPatcherInterface
{
    public function applyPatch(BpsPatch $patch, string $sourceFilePath, string $targetFilePath, ?callable $onProgress = null): void
    {
        $sourceHash = hash_file('crc32b', $sourceFilePath);
        $metadata = $patch->getMetadata();
        if (hexdec($sourceHash) !== $metadata->getChecksumSource()) {
            throw new PatchSourceException('The patch is not intended for this source');
        }

        $patchFile = $patch->getPatchFile();
        $source = File::load($sourceFilePath)->getFileContents();
        $target = str_repeat("\0", $metadata->getTargetSize());

        $offsetSourceRelative = 0;
        $offsetTargetRelative = 0;
        $outputOffset = 0;

        $onProgress && $onProgress(0, $metadata->getTargetSize());

        foreach ($patch->iterateCommands() as $command) {
            switch ($command->getCommandType()) {
                case BpsCommandType::SourceRead:
                    $this->sourceRead($command, $outputOffset, $source, $target);
                    break;
                case BpsCommandType::TargetRead:
                    $this->tragetRead($command, $outputOffset, $patchFile, $target);
                    break;
                case BpsCommandType::SourceCopy:
                    $this->sourceCopy($command, $outputOffset, $patchFile, $source, $target, $offsetSourceRelative);
                    break;
                case BpsCommandType::TargetCopy:
                    $this->targetCopy($command, $outputOffset, $patchFile, $target, $offsetTargetRelative);
                    break;
            }

            /** @noinspection DisconnectedForeachInstructionInspection */
            $onProgress && $onProgress($outputOffset, $metadata->getTargetSize());
        }

        file_put_contents($targetFilePath, $target);

        $onProgress && $onProgress($metadata->getTargetSize(), $metadata->getTargetSize());

        $targetHash = hash_file('crc32b', $sourceFilePath);
        if (hexdec($targetHash) !== $metadata->getChecksumSource()) {
            throw new PatchTargetException('The patch was not applied successfully: Checksum mismatch');
        }
    }

    private function sourceRead(BpsCommand $command, int &$outputOffset, string $source, string &$target): void
    {
        $length = $command->getLength();

        while ($length--) {
            $target[$outputOffset] = $source[$outputOffset];
            $outputOffset++;
        }
    }

    private function tragetRead(BpsCommand $command, int &$outputOffset, File $patchFile, string &$target): void
    {
        $length = $command->getLength();

        while ($length--) {
            $target[$outputOffset++] = $patchFile->readChar();
        }
    }

    private function sourceCopy(BpsCommand $command, int &$outputOffset, File $patchFile, string $source, string &$target, int &$offsetSourceRelative): void
    {
        $data = BpsHelper::decodeNumber($patchFile);

        // Lowest byte indicates sign
        $offsetSourceRelative += (($data & 1) === 1 ? -1 : +1) * ($data >> 1);

        $length = $command->getLength();

        while ($length--) {
            $target[$outputOffset++] = $source[$offsetSourceRelative++];
        }
    }

    private function targetCopy(BpsCommand $command, int &$outputOffset, File $patchFile, string &$target, int &$offsetTargetRelative): void
    {
        $data = BpsHelper::decodeNumber($patchFile);

        // Lowest byte indicates sign
        $offsetTargetRelative += (($data & 1) === 1 ? -1 : +1) * ($data >> 1);

        $length = $command->getLength();

        while ($length--) {
            $target[$outputOffset++] = $target[$offsetTargetRelative++];
        }
    }
}
