<?php

declare(strict_types=1);

namespace FlyingAnvil\Bepes;

use FlyingAnvil\Bepes\DataObject\BpsCommand;
use FlyingAnvil\Bepes\DataObject\BpsPatchMetadata;
use FlyingAnvil\Bepes\Exception\PatchFileException;
use FlyingAnvil\Libfa\Wrapper\File;
use Generator;

class BpsPatch
{
    public const MAGIC_BYTES = 'BPS1';

    private File $file;

    private BpsPatchMetadata $metadata;

    private function __construct(
        private string $patchFilePath,
    ) {
        $this->file = File::load($this->patchFilePath);
        $this->file->open();

        if (!$this->verifyType()) {
            throw new PatchFileException('File is not a valid bps patch: magic bytes mismatch');
        }

        $this->metadata = $this->parseMetadata();

        if (!$this->verifyIntegrity()) {
            throw new PatchFileException('Patch is corrpted: checksum mismatch');
        }
    }

    public static function load(string $patchFilePath): self
    {
        return new self($patchFilePath);
    }

    private function verifyType(): bool
    {
        $this->file->rewind();
        $read = $this->file->read(4);

        return $read === self::MAGIC_BYTES;
    }

    private function verifyIntegrity(): bool
    {
        $this->file->rewind();

        $tempFilePath = tempnam(sys_get_temp_dir(), 'bepes');
        $this->file->copy($tempFilePath);

        $tempFile = fopen($tempFilePath, 'r+b');
        ftruncate($tempFile, $this->file->getFileSize() - 4);
        fclose($tempFile);

        $patchFileHash = hash_file('crc32b', $tempFilePath, false);
        unlink($tempFilePath);

        return hexdec($patchFileHash) === $this->metadata->getChecksumPatch();
    }

    private function parseMetadata(): BpsPatchMetadata
    {
        $this->file->seek(4);

        $sourceSize   = BpsHelper::decodeNumber($this->file);
        $targetSize   = BpsHelper::decodeNumber($this->file);
        $metadataSize = BpsHelper::decodeNumber($this->file);
        $metadata     = $metadataSize > 0 ? $this->file->read($metadataSize) : '';

        $commandsStart = $this->file->tell();

        $this->file->seek(-12, SEEK_END);
        $checksumSource = $this->file->readUInt32LittleEndian();
        $checksumTarget = $this->file->readUInt32LittleEndian();
        $checksumPatch  = $this->file->readUInt32LittleEndian();

        return BpsPatchMetadata::create(
            sourceSize: $sourceSize,
            targetSize: $targetSize,
            metadataSize: $metadataSize,
            metadata: $metadata,
            offsetCommands: $commandsStart,
            checksumSource: $checksumSource,
            checksumTarget: $checksumTarget,
            checksumPatch: $checksumPatch,
        );
    }

    /**
     * @return Generator | BpsCommand
     */
    public function iterateCommands(): Generator
    {
        $this->file->seek($this->metadata->getOffsetCommands());
        $readUntil = $this->file->getFileSize() - 12;

        while ($this->file->tell() < $readUntil) {

//            echo PHP_EOL;
//            echo 'Patch Pos Pre:  ' . $this->file->tell(), PHP_EOL;
            $rawData = BpsHelper::decodeNumber($this->file);
//            echo 'Patch Pos Post: ' . $this->file->tell(), PHP_EOL;
//            echo 'Decoded Number: ' . $rawData, PHP_EOL;

            $bpsCommand = BpsCommand::createFromRaw($rawData);
//            var_dump($bpsCommand->getLength());

            yield $bpsCommand;
        }
    }

    /**
     * @return File already opened as rb
     */
    public function getPatchFile(): File
    {
        return $this->file;
    }

    /**
     * @internal Used by the patcher for the TargetRead command
     */
    public function readChar(): string
    {
        return $this->file->readChar();
    }

    public function getMetadata(): BpsPatchMetadata
    {
        return $this->metadata;
    }
}
