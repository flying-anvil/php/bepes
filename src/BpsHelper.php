<?php

declare(strict_types=1);

namespace FlyingAnvil\Bepes;

use FlyingAnvil\Libfa\Wrapper\File;
use RuntimeException;

class BpsHelper
{
    public static function decodeNumber(File $file): int
    {
        $data  = 0;
        $shift = 1;

        for ($i = 0; $i < 10; $i++)
        {
            $byte = $file->readUnsignedByte();
            $data += ($byte & 0b0111_1111) * $shift;

            // Highest bit indicates we're done
            if ($byte & 0b1000_0000) {
                return $data;
            }

            $shift <<= 7;
            $data += $shift;
        }

        throw new RuntimeException('Infinite Loop while decoding number');
    }

    public static function encodeNumber(int $number): string
    {
        $result = '';

        while (true)
        {
            $byte = $number & 0b0111_1111;
            $number >>= 7;

            if ($number === 0) {
                return $result;
            }

            $result .= chr($byte);
            $number--;
        }
    }
}
