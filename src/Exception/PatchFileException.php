<?php

declare(strict_types=1);

namespace FlyingAnvil\Bepes\Exception;

class PatchFileException extends BepesException
{
}
