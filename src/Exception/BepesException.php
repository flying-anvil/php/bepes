<?php

declare(strict_types=1);

namespace FlyingAnvil\Bepes\Exception;

use FlyingAnvil\Libfa\Exception\FlyingAnvilException;

class BepesException extends FlyingAnvilException
{
}
