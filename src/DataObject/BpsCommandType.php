<?php

declare(strict_types=1);

namespace FlyingAnvil\Bepes\DataObject;

use FlyingAnvil\Libfa\DataObject\DataObject;
use JetBrains\PhpStorm\Immutable;
use JetBrains\PhpStorm\Internal\TentativeType;

#[Immutable]
enum BpsCommandType: int implements DataObject
{
    // Reads next byte from the source into the target
    case SourceRead = 0;

    // Reads next byte from the patch into the target
    case TargetRead = 1;

    // Read relative offset from the source into the target
    case SourceCopy = 2;

    // Read relative offset from the target (already written data) into the target
    case TargetCopy = 3;

    public function jsonSerialize(): int
    {
        return $this->value;
    }
}
