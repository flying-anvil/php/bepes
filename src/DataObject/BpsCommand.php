<?php

declare(strict_types=1);

namespace FlyingAnvil\Bepes\DataObject;

use Closure;
use FlyingAnvil\Libfa\DataObject\DataObject;
use JetBrains\PhpStorm\Immutable;

#[Immutable]
class BpsCommand implements DataObject
{
    private function __construct(
        private BpsCommandType $commandType,
        private int            $length,
    ) {}

    public static function create(BpsCommandType $commandType, int $length): self
    {
        return new self($commandType, $length);
    }

    public static function createFromRaw(int $rawData): self
    {
        // Lowest 2 bit are the type, so they must be isolated and shifted out.
        $commandType = BpsCommandType::from($rawData & 0b0000_0011);
        $length = ($rawData >> 2) + 1;

        return new self($commandType, $length);
    }

    public function getCommandType(): BpsCommandType
    {
        return $this->commandType;
    }

    public function getLength(): int
    {
        return $this->length;
    }

    public function jsonSerialize(): array
    {
        return [
            'commandType' => $this->commandType,
            'length'      => $this->length,
        ];
    }
}
