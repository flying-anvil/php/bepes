<?php

declare(strict_types=1);

namespace FlyingAnvil\Bepes\DataObject;

use FlyingAnvil\Libfa\DataObject\DataObject;
use JetBrains\PhpStorm\Immutable;

#[Immutable]
final class BpsPatchMetadata implements DataObject
{
    private function __construct(
        private int    $sourceSize,
        private int    $targetSize,
        private int    $metadataSize,
        private string $metadata,
        private int    $offsetCommands,
        private int    $checksumSource,
        private int    $checksumTarget,
        private int    $checksumPatch,
    ) {}

    public static function create(
        int $sourceSize,
        int $targetSize,
        int $metadataSize,
        string $metadata,
        int $offsetCommands,
        int $checksumSource,
        int $checksumTarget,
        int $checksumPatch,
    ): self {
        return new self(
            $sourceSize,
            $targetSize,
            $metadataSize,
            $metadata,
            $offsetCommands,
            $checksumSource,
            $checksumTarget,
            $checksumPatch,
        );
    }

    public function getSourceSize(): int
    {
        return $this->sourceSize;
    }

    public function getTargetSize(): int
    {
        return $this->targetSize;
    }

    public function getMetadataSize(): int
    {
        return $this->metadataSize;
    }

    public function getMetadata(): string
    {
        return $this->metadata;
    }

    public function getOffsetCommands(): int
    {
        return $this->offsetCommands;
    }

    public function getChecksumSource(): int
    {
        return $this->checksumSource;
    }

    public function getChecksumTarget(): int
    {
        return $this->checksumTarget;
    }

    public function getChecksumPatch(): int
    {
        return $this->checksumPatch;
    }

    public function jsonSerialize(): array
    {
        return [
            'sourceSize'     => $this->sourceSize,
            'targetSize'     => $this->targetSize,
            'metadataSize'   => $this->metadataSize,
            'metadata'       => $this->metadata,
            'offsetCommand'  => $this->offsetCommands,
            'checksumSource' => $this->checksumSource,
            'checksumTarget' => $this->checksumTarget,
            'checksumPatch'  => $this->checksumPatch,
        ];
    }
}
